﻿/*
	Необходимо рассчитать стоимость материалов для бетонной дорожки вокруг круглого бассейна, а также стоимость материалов ограды вокруг бассейна (вместе с дорожкой). Стоимость 1 квадратного метра бетонного покрытия 1000 р. Стоимость 1 погонного метра ограды 2000 р. Радиус бассейна 3 м. Ширина бетонной дорожки вокруг бассейна 1 м.
*/
#include <iostream>
#include <locale.h>
#include "task06-1.h"
#define POOL_RADIUS 3.0
#define PATH_RADIUS 1.0
#define COVER_COST 1000.0
#define FENCE_COST 2000.0

using namespace std;

int main() {

	double cost = 0.0;

	setlocale(LC_ALL, "rus");

	Circle pool_territory(POOL_RADIUS + PATH_RADIUS);
	Circle pool(POOL_RADIUS);

	cost += ((pool_territory.get_area() - pool.get_area()) * COVER_COST);
	cost += (pool_territory.get_ference() * FENCE_COST);

	printf("Ответ: %.2f р", cost);

	system("pause");
	return 0;
}