#include "task06-1.h"
#include <iostream>
#define FENCE_PRICE 2000
#define COATING_PRICE 1000
using namespace std;
int main()
{
	double cost_track, cost_fence;
	Circle fence(4.0);
	Circle pool(3.0);
	cost_track = (fence.get_Area() - pool.get_Area()) * COATING_PRICE;
	cost_fence = fence.get_Ference() * FENCE_PRICE;
	cout << "Cost track: " << cost_track << " rub." << endl;
	cout << "Cost fence: " << cost_fence << " rub." << endl;
	return 0;
}
