#ifndef TASK06-1_H
#define TASK06-1_H
#include <math.h>
class Circle
{
    private:
        double Radius;
        double Ference;
        double Area;
    public:
        Circle(double r)
        {
		    Radius = r;
			Ference = 2 * M_PI * r;
			Area = M_PI * r * r;
        }
        void set_Radius(double r)
        {
            Radius = r;
            Ference = 2 * Radius * M_PI;
            Area = M_PI * Radius * Radius;
        }
        void set_Ference(double f)
        {
            Ference = f;
            Radius = Ference / 2 / M_PI;
            Area = M_PI * Radius * Radius;
        }
        void set_Area(double ar)
        {
            Area = ar;
            Radius = sqrt( Area / M_PI );
            Ference = 2 * Radius * M_PI;
        }
        double get_Radius()
        {
            return Radius;
        }
        double get_Ference()
        {
            return Ference;
        }
        double get_Area()
        {
            return Area;
        }
};
#endif

