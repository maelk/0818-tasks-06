#include <math.h>

class Circle {
private:
	double Radius, Ference, Area;

public:
	Circle(double rad = 0)
	{
		Radius = rad;
		Ference = 2 * M_PI * Radius;
		Area = M_PI * Radius * Radius;
	}

	void setRadius(double _Radius)
	{
		Radius = _Radius;
		Ference = 2 * M_PI * Radius;
		Area = M_PI * Radius * Radius;
	}

	void setFerence(double _Ference)
	{
		Ference = _Ference;
		Radius = Ference / (2 * M_PI);
		Area = M_PI * Radius * Radius;
	}

	void setArea(double _Area)
	{
		Area = _Area;
		Radius = sqrt(Area / M_PI);
		Ference = 2 * M_PI * Radius;
	}

	double getRadius()const
	{
		return Radius;
	}

	double getFerence()const
	{
		return Ference;
	}

	double getArea()const
	{
		return Area;
	}
};
